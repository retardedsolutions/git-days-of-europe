ideas = {
	country = {

		VIN_vietcong_idea = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea VIN_vietcong_idea"}

			allowed = {
				always = no
			}

			modifier = {
				land_reinforce_rate = -0.015
				communist_drift = 0.01
				political_power_gain = -0.10
			}
		}

		VIN_agrarian_society = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea VIN_agrarian_society"}

			allowed = {
				always = no
			}

			modifier = {
				production_speed_buildings_factor = -0.15
				consumer_goods_use_modifier = 0.07
			}
		}

		VIN_japanese_rubber_exports = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea VIN_japanese_rubber_exports"}

			allowed = {
				always = no
			}

			modifier = {
				consumer_goods_use_modifier = 0.05
			}
		}

		VIN_japanese_army = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea VIN_japanese_army"}

			allowed = {
				always = no
			}

			modifier = {
				conscription = -0.1
			}
		}

		VIN_Crippling_Illiteracy = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea VIN_Crippling_Illiteracy"}
			picture = CHI_Elite_Private_Education2
			allowed = {
				always = no
			}


			modifier = {
				research_speed_factor = -0.15
				training_time_army_factor = 0.15
			}
		}

		VIN_japanese_industry_nationalized_JAP = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea VIN_japanese_industry_nationalized_JAP"}

			allowed = {
				always = no
			}

			modifier = {
				# consumer_goods_use_modifier = 0.05
			}
		}

		VIN_japanese_industry_nationalized_VIN = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea VIN_japanese_industry_nationalized_VIN"}

			allowed = {
				always = no
			}

			modifier = {
				# consumer_goods_use_modifier = -0.05
			}
		}

		VIN_vietnamese_taxation_JAP = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea VIN_vietnamese_taxation_JAP"}

			allowed = {
				always = no
			}

			modifier = {
				# consumer_goods_use_modifier = -0.05
			}
		}

		VIN_vietnamese_taxation_VIN = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea VIN_vietnamese_taxation_VIN"}

			allowed = {
				always = no
			}

			modifier = {
				# consumer_goods_use_modifier = 0.05
			}
		}

		VIN_tax_japanese_industry_idea = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea VIN_tax_japanese_industry_idea"}

			allowed = {
				always = no
			}

			modifier = {
				# consumer_goods_use_modifier = 0.05
			}
		}
	}
}
