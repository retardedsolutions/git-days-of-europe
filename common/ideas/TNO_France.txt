ideas = {
	country = {
		FRA_economic_meltdown = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_economic_meltdown" }

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = GFX_idea_FRA_economic_meltdown

			removal_cost = -1

			modifier = {
				production_speed_buildings_factor = -0.2
				industrial_capacity_factory = -0.2
				production_factory_efficiency_gain_factor = -0.2
				trade_opinion_factor = -0.5
			}
		}

		FRA_the_OAS = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_the_OAS" }

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = GFX_idea_FRA_the_OAS

			removal_cost = -1

			modifier = {
				compliance_gain = 0.1
				stability_factor = 0.1
				political_power_factor = -0.1
			}
		}

		FRA_the_treaty_of_vichi = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_the_treaty_of_vichi" }

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = GFX_idea_FRA_the_treaty_of_vichi

			removal_cost = -1

			equipment_bonus = {
				infantry_equipment = {
					build_cost_ic = 0.6 instant = yes
				}
			}

			modifier = {
                production_factory_max_efficiency_factor = -0.5
				conscription_factor = -0.5
				research_speed_factor = -0.1
                army_speed_factor = -0.8
				amphibious_invasion = -0.8
				naval_invasion_capacity = -100
                custom_modifier_tooltip = FRA_vichy_restriction_tt
				training_time_factor = 0.3
			}
		}

		FRA_les_annees_noires = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea FRA_les_annees_noires" }

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			picture = GFX_idea_FRA_les_annees_noires

			removal_cost = -1

			modifier = {
				stability_factor = -0.2
				political_power_factor = -0.3
				gdp_growth_modifier = -0.05
				monthly_credit_rating_progress = -1
			}
		}
	}
}
