ideas = {

	country = {
		MEX_mexican_dictatorship = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea MEX_mexican_dictatorship"}
			allowed = {
				always = yes
			}
			picture = MEX_The_Imperfect_Dictatorship
			removal_cost = -1
			modifier = {
				political_power_gain = 0.25
				# +25% PRI Loyalty Base
				stability_factor = -0.05
			}
		}

		MEX_mexican_miracle_dummy = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea MEX_mexican_miracle_dummy"}
			allowed = {
				always = yes
			}
			picture = MEX_mexican_miracle
			removal_cost = -1
			modifier = {
				trade_opinion_factor = 0.1
				monthly_population = 0.05
				gdp_growth_modifier = 2.5
			}
		}

		MEX_rural_depression = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea MEX_rural_depression"}
			allowed = {
				always = yes
			}
			#picture = MEX_rural_depression
			removal_cost = -1
			modifier = {
				stability_factor = -0.10
				poverty_monthly_rate = -0.05
				taxable_population_factor = -0.10
				# Peasantry Loyalty Base -20%
			}
		}

		MEX_the_leviathan_4 = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea MEX_the_leviathan_4"}
			allowed = {
				always = yes
			}
			picture = MEX_the_leviathan_4
			removal_cost = -1
			modifier = {
				civilian_expenditures_factor = 0.15
				#admin_efficiency_monthly_rate = -0.1
				free_production_units_modifier = -5
				research_speed_factor = -0.10
			}
		}

		MEX_Economic_Battlefield = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea the_zaibatsus"}

			allowed = {
				always = yes
			}

			picture = USA_gld_68_tariff_oofing

			removal_cost = -1

			modifier = {
				consumer_goods_use_modifier = 0.1
				trade_opinion_factor = 0.1
				# min_export = 0.1
			}
		}

		MEX_Fraying_Ideals = {
			on_add = {log = "[GetDateText]: [Root.GetName]: add idea the_zaibatsus"}

			allowed = {
				always = yes
			}

			picture = a_crusade_against_corruption

			removal_cost = -1

			modifier = {
				political_power_cost = -0.15
				stability_factor = -0.2

			}
		}
	}
}
