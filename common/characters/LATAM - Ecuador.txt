characters = {
	ECU_Jose_Maria_Velasco_Ibarra = {
		name = ECU_Jose_Maria_Velasco_Ibarra
		portraits = { civilian = { large = "gfx/leaders/ECU/Portrait_Ecuador_Jose_Ibarra.dds" } }
		country_leader = {
			expire = "1994.4.20"
			ideology = authoritarian_democracy_dominant_party_democracy_subtype
			traits = { }
			id = -1
		}
	}
}