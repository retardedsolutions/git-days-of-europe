characters = {
	COL_Alberto_Lleras_Camargo = {
		name = COL_Alberto_Lleras_Camargo
		portraits = { civilian = { large = "gfx/leaders/COL/Portrait_Colombia_Alberto_Camargo.dds" } }
		country_leader = {
			expire = "1994.4.20"
			ideology = liberal_democracy_subtype
			traits = { }
			id = -1
		}
	}
}