characters = {
	VIN_ho_chi_minh = {
		name = VIN_ho_chi_minh
		portraits = { 
			civilian = { large = "gfx/leaders/VIN/Portrait_Vietnam_Ho_Chi_Minh.dds" }
		}
		country_leader = {
			desc = "POLITICS_HO_CHI_MINH_DESC"
			ideology = communist_bukharinism_subtype
			traits = {}
			expire = "1999.12.12"
			id = -1
		}
	}
	VIN_bao_dai = {
		name = VIN_bao_dai
		portraits = { 
			civilian = { large = "gfx/leaders/VIN/Portrait_Vietnam_Bao_Dai.dds" }
		}
		country_leader = {
			desc = "POLITICS_BAO_DAI_DESC"
			ideology = despotism_absolute_monarchy_subtype
			traits = {}
			expire = "1999.12.12"
			id = -1
		}
		country_leader = {
			desc = "POLITICS_BAO_DAI_DESC"
			ideology = social_democracy_subtype
			traits = {}
			expire = "1999.12.12"
			id = -1
		}
		country_leader = {
			desc = "POLITICS_BAO_DAI_DESC"
			ideology = liberal_democracy_subtype
			traits = {}
			expire = "1999.12.12"
			id = -1
		}
		country_leader = {
			desc = "POLITICS_BAO_DAI_DESC"
			ideology = conservative_democracy_subtype
			traits = {}
			expire = "1999.12.12"
			id = -1
		}
		country_leader = {
			desc = "POLITICS_BAO_DAI_DESC"
			ideology = authoritarian_democracy_subtype
			traits = {}
			expire = "1999.12.12"
			id = -1
		}
	}
	VIN_ngo_dinh_diem = {
		name = VIN_ngo_dinh_diem
		portraits = { 
			civilian = { large = "gfx/leaders/VIN/Portrait_Vietnam_Ngo_Diem.dds" }
		}
		country_leader = {
			#desc = ""
			ideology = fascism_subtype
			traits = {}
			expire = "1999.12.12"
			id = -1
		}
	}
	VIN_nguyen_cao_ky = {
		name = VIN_nguyen_cao_ky
		portraits = { 
			civilian = { large = "gfx/leaders/VIN/Portrait_Vietnam_Nguyen_Cao_Ky.dds" }
		}
		country_leader = {
			#desc = ""
			ideology = national_socialism_subtype
			traits = {}
			expire = "1999.12.12"
			id = -1
		}
	}
	VIN_truong_chinh = {
		name = VIN_truong_chinh
		portraits = { 
			civilian = { large = "gfx/leaders/VIN/Portrait_Vietnam_Truong_Chinh.dds" }
		}
		country_leader = {
			desc = "POLITICS_TRUONG_CHINH_DESC"
			ideology = communist_mao_zedong_thought_subtype
			traits = {}
			expire = "1999.12.12"
			id = -1
		}
	}
}