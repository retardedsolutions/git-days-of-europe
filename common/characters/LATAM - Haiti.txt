characters = {
	HAI_Francois_Duvalier = {
		name = HAI_Francois_Duvalier
		portraits = { civilian = { large = "gfx/leaders/HAI/Portrait_Haiti_Francois_Duvalier.dds" } }
		country_leader = {
			expire = "1994.4.20"
			ideology = despotism_personalistic_dictatorship_subtype
			traits = { }
			id = -1
		}
	}
	HAI_Rene_Theodore = {
		name = HAI_Rene_Theodore
		portraits = {
			civilian = {
				#large = "gfx/leaders/HAI/Portrait_Haiti_Rene_Theodore.dds"
			}
		}
		country_leader = {
			expire = "1994.4.20"
			ideology = communist_subtype
			traits = { }
			id = -1
		}
	}
	HAI_Jean_Claude_Duvalier = {
		name = HAI_Jean_Claude_Duvalier
		portraits = { 
			civilian = { 
				large = "gfx/leaders/HAI/Portrait_Haiti_Jean-Claude_Duvalier.dds"
			}
		}
		country_leader = {
			expire = "2000.1.1"
			ideology = despotism_personalistic_dictatorship_subtype
			traits = { }
			id = -1
		}
	}
}