characters = {
	AFG_mohammed_zahir_shah = {
		name = AFG_mohammed_zahir_shah
		portraits = {
			civilian = { large = "gfx/leaders/AFG/Portrait_Afghanistan_Mohammed_Zahir_Shah.dds" }
		}
		country_leader = {
			desc = "POLITICS_MOHAMMED_ZAHIR_SHAH_DESC"
			expire = "1999.1.1"
			ideology = authoritarian_democracy_semi_constitutional_monarchy_subtype
			traits = {}
			id = -1
		}
	}
	AFG_mohammad_musa_shafiq = {
		name = AFG_mohammad_musa_shafiq
		portraits = {
			civilian = { large = "gfx/leaders/AFG/Portrait_Afghanistan_Mohammad_Musa_Shafiq.dds" }
		}
		country_leader = {
			#desc = "POLITICS_MOHAMMAD_MUSA_SHAFIQ_DESC"
			expire = "1999.1.1"
			ideology = liberal_democracy_subtype
			traits = {}
			id = -1
		}
	}
	AFG_abdul_hadi_dawi = {
		name = AFG_abdul_hadi_dawi
		portraits = {
			civilian = { large = "GFX_leader_unknown" }
		}
		country_leader = {
			#desc = "POLITICS_ABDUL_HADI_DAWI_DESC"
			expire = "1999.1.1"
			ideology = conservative_democracy_subtype
			traits = {}
			id = -1
		}
	}
	AFG_gulbuddin_hekmatyar = {
		name = AFG_gulbuddin_hekmatyar
		portraits = {
			civilian = { large = "gfx/leaders/AFG/Portrait_Afghanistan_Gulbuddin_Hekmatyar.dds" }
		}
		country_leader = {
			#desc = "POLITICS_GULBUDDIN_HEKMATYAR_DESC"
			expire = "1999.1.1"
			ideology = ultranationalism_subtype
			traits = {}
			id = -1
		}
	}
	AFG_mohammad_daoud_khan = {
		name = AFG_mohammad_daoud_khan
		portraits = {
			civilian = {
				large = "gfx/leaders/AFG/Portrait_Afghanistan_Mohammad_Daoud_Khan.dds" 
				small = "GFX_idea_AFG_Mohammad_Daoud_Khan_hog" 
			}
		}
		country_leader = {
			#desc = "POLITICS_MOHAMMAD_DAOUD_KHAN_DESC"
			expire = "1999.1.1"
			ideology = social_democracy_subtype
			traits = {}
			id = -1
		}
		advisor = {
			slot = head_of_government
			idea_token = AFG_mohammad_daoud_khan_hog
			allowed = { always = yes }
			traits = { hog_local_tyrant }
			ledger = civilian
			cost = -1
			removal_cost = -1
			ai_will_do = { factor = 0 }
		}
	}
	AFG_mohammad_hashim_maiwandwal = {
		name = AFG_mohammad_hashim_maiwandwal
		portraits = {
			civilian = { 
				small = "GFX_idea_AFG_Mohammad_Hashim_Maiwandwal_for" 
			}
		}
		advisor = {
			slot = foreign_minister
			idea_token = AFG_mohammad_hashim_maiwandwal_for
			allowed = { always = yes }
			traits = { for_great_compromiser }
			ledger = civilian
			cost = -1
			removal_cost = -1
			ai_will_do = { factor = 0 }
		}
	}
	AFG_sardar_shah_wali_khan = {
		name = AFG_sardar_shah_wali_khan
		portraits = {
			civilian = { 
				small = "GFX_idea_AFG_Sardar_Shah_Wali_Khan_sec" 
			}
		}
		advisor = {
			slot = security_minister
			idea_token = AFG_sardar_shah_wali_khan_sec
			allowed = { always = yes }
			traits = { sec_aggressive_fighter }
			ledger = civilian
			cost = -1
			removal_cost = -1
			ai_will_do = { factor = 0 }
		}
	}
	KLT_mir_ahmad_yar_khan = {
		name = KLT_mir_ahmad_yar_khan
		portraits = {
			civilian = { large = "gfx/leaders/KLT/Portrait_Kalat_Mir_Ahmad_Yar_Khan.dds" }
		}
		country_leader = {
			desc = "POLITICS_MIR_AHMAD_YAR_KHAN_DESC"
			expire = "1999.1.1"
			ideology = authoritarian_democracy_semi_constitutional_monarchy_subtype
			traits = {}
			id = -1
		}
	}
	KLT_sher_mohammed_marri = {
		name = KLT_sher_mohammed_marri
		portraits = {
			civilian = { large = "GFX_leader_unknown" }
		}
		country_leader = {
			desc = "POLITICS_SHER_MOHAMMED_MARRI_DESC"
			expire = "1999.1.1"
			ideology = communist_subtype
			traits = {}
			id = -1
		}
	}
	KLT_mir_gul_khan_nasseer = {
		name = KLT_mir_gul_khan_nasseer
		portraits = {
			civilian = { large = "GFX_leader_unknown" }
		}
		country_leader = {
			#desc = ""
			expire = "1999.1.1"
			ideology = conservative_democracy_subtype
			traits = {}
			id = -1
		}
	}
	KLT_khair_bakhsh_marri = {
		name = KLT_khair_bakhsh_marri
		portraits = {
			civilian = { large = "GFX_leader_unknown" }
		}
		country_leader = {
			#desc = ""
			expire = "1999.1.1"
			ideology = socialist_subtype
			traits = {}
			id = -1
		}
	}
	KLT_abdul_wali = {
		name = KLT_abdul_wali
		portraits = {
			civilian = { large = "GFX_leader_unknown" }
		}
		country_leader = {
			#desc = ""
			expire = "1999.1.1"
			ideology = despotism_subtype
			traits = {}
			id = -1
		}
	}
	KLT_mohammad_daoud_khan = {
		name = AFG_mohammad_daoud_khan
		portraits = {
			civilian = {
				large = "gfx/leaders/AFG/Portrait_Afghanistan_Mohammad_Daoud_Khan.dds" 
				small = "GFX_idea_AFG_Mohammad_Daoud_Khan_hog" 
			}
		}
		advisor = {
			slot = head_of_government
			idea_token = KLT_mohammad_daoud_khan_hog
			allowed = { always = yes }
			traits = { hog_local_tyrant }
			ledger = civilian
			cost = -1
			removal_cost = -1
			ai_will_do = { factor = 0 }
		}
	}
	KLT_mohammad_hashim_maiwandwal = {
		name = AFG_mohammad_hashim_maiwandwal
		portraits = {
			civilian = { 
				small = "GFX_idea_AFG_Mohammad_Hashim_Maiwandwal_for" 
			}
		}
		advisor = {
			slot = foreign_minister
			idea_token = KLT_mohammad_hashim_maiwandwal_for
			allowed = { always = yes }
			traits = { for_great_compromiser }
			ledger = civilian
			cost = -1
			removal_cost = -1
			ai_will_do = { factor = 0 }
		}
	}
	KLT_sardar_shah_wali_khan = {
		name = AFG_sardar_shah_wali_khan
		portraits = {
			civilian = { 
				small = "GFX_idea_AFG_Sardar_Shah_Wali_Khan_sec" 
			}
		}
		advisor = {
			slot = security_minister
			idea_token = KLT_sardar_shah_wali_khan_sec
			allowed = { always = yes }
			traits = { sec_aggressive_fighter }
			ledger = civilian
			cost = -1
			removal_cost = -1
			ai_will_do = { factor = 0 }
		}
	}
	MFP_nawabzada_shahabuddin_khan = {
		name = MFP_nawabzada_shahabuddin_khan
		portraits = {
			civilian = { large = "gfx/leaders/MFP/Portrait_Frontier_Provinces_Nawabzada_Shahabuddin_Khan.dds" }
		}
		country_leader = {
			desc = "POLITICS_NAWABZADA_SHAHABUDDIN_KHAN_DESC"
			expire = "1999.1.1"
			ideology = despotism_aristocratic_conservatism_subtype
			traits = {}
			id = -1
		}
	}
	MFP_muhammad_akbar_khan = {
		name = MFP_muhammad_akbar_khan
		portraits = {
			civilian = { large = "GFX_leader_unknown" }
		}
		country_leader = {
			#desc = ""
			expire = "1999.1.1"
			ideology = authoritarian_democracy_subtype
			traits = {}
			id = -1
		}
	}
	MFP_pashtun_tribal_clans = {
		name = MFP_pashtun_tribal_clans
		portraits = {
			civilian = { large = "GFX_leader_unknown" }
		}
		country_leader = {
			#desc = ""
			expire = "1999.1.1"
			ideology = conservative_democracy_subtype
			traits = {}
			id = -1
		}
	}
	MFP_mohammad_daoud_khan = {
		name = AFG_mohammad_daoud_khan
		portraits = {
			civilian = {
				large = "gfx/leaders/AFG/Portrait_Afghanistan_Mohammad_Daoud_Khan.dds" 
				small = "GFX_idea_AFG_Mohammad_Daoud_Khan_hog" 
			}
		}
		advisor = {
			slot = head_of_government
			idea_token = MFP_mohammad_daoud_khan_hog
			allowed = { always = yes }
			traits = { hog_local_tyrant }
			ledger = civilian
			cost = -1
			removal_cost = -1
			ai_will_do = { factor = 0 }
		}
	}
	MFP_mohammad_hashim_maiwandwal = {
		name = AFG_mohammad_hashim_maiwandwal
		portraits = {
			civilian = { 
				small = "GFX_idea_AFG_Mohammad_Hashim_Maiwandwal_for" 
			}
		}
		advisor = {
			slot = foreign_minister
			idea_token = MFP_mohammad_hashim_maiwandwal_for
			allowed = { always = yes }
			traits = { for_great_compromiser }
			ledger = civilian
			cost = -1
			removal_cost = -1
			ai_will_do = { factor = 0 }
		}
	}
	MFP_sardar_shah_wali_khan = {
		name = AFG_sardar_shah_wali_khan
		portraits = {
			civilian = { 
				small = "GFX_idea_AFG_Sardar_Shah_Wali_Khan_sec" 
			}
		}
		advisor = {
			slot = security_minister
			idea_token = MFP_sardar_shah_wali_khan_sec
			allowed = { always = yes }
			traits = { sec_aggressive_fighter }
			ledger = civilian
			cost = -1
			removal_cost = -1
			ai_will_do = { factor = 0 }
		}
	}
}