characters = {
	# Leaders
	VOL_Vasily_Ivanov = {
		name = VOL_Vasily_Ivanov
		portraits = {
			civilian = {
				large = "gfx/leaders/VOL/Portrait_Vologda_Vasily_Ivanov.dds"
				#small = "GFX_idea_VOL_Vasily_Ivanov"
			}
			army = {
				large = "gfx/leaders/VOL/Portrait_Vologda_Vasily_Ivanov.dds"
				#small = "GFX_idea_VOL_Vasily_Ivanov"
			}
		}

		country_leader = {
			desc = "POLITICS_VASILY_IVANOV_DESC"
			expire = "1999.1.1"
			ideology = authoritarian_democracy_controlled_democracy_subtype
			traits = { }
			id = -1
		}

		field_marshal = {
			traits = { war_hero trait_cautious defensive_doctrine }
			skill = 3
			attack_skill = 1
			defense_skill = 3
			planning_skill = 3
			logistics_skill = 3
			legacy_id = 3421
		}
	}

	# Ministers
	VOL_Vasily_Belov = {
		name = VOL_Vasily_Belov
		portraits = {
			civilian = {
				small = "GFX_idea_VOL_minister_Vasily_Belov"
			}
		}

		advisor = {
			slot = head_of_government
			idea_token = VOL_Vasily_Belov_hog
			allowed = { original_tag = VOL }
			traits = {
				head_of_government
				hog_pragmatic_statesman
			}
			ledger = civilian
			cost = -1
			removal_cost = -1
			ai_will_do = { factor = 0 }
		}
	}

	VOL_Sergey_Vikulov = {
		name = VOL_Sergey_Vikulov
		portraits = {
			civilian = {
				small = "GFX_idea_VOL_minister_Sergey_Vikulov"
			}
		}

		advisor = {
			slot = foreign_minister
			idea_token = VOL_Sergey_Vikulov_for
			allowed = { original_tag = VOL }
			traits = {
				foreign_minister
				for_biased_intellectual
			}
			ledger = civilian
			cost = -1
			removal_cost = -1
			ai_will_do = { factor = 0 }
		}
	}

	VOL_Sergey_Ilyushin = {
		name = VOL_Sergey_Ilyushin
		portraits = { civilian = { small = "GFX_idea_VOL_minister_Sergey_Ilyushin" } }

		advisor = {
			slot = economy_minister
			idea_token = VOL_Sergey_Ilyushin_eco
			allowed = { original_tag = VOL }
			traits = {
				economy_minister
				eco_battlefield_support_proponent
			}
			ledger = civilian
			cost = -1
			removal_cost = -1
			ai_will_do = { factor = 0 }
		}
	}

	VOL_Pavel_Belyayev = {
		name = VOL_Pavel_Belyayev
		portraits = {
			civilian = {
				large = "gfx/leaders/VOL/Portrait_Vologda_Pavel_Belyayev.dds"
				small = "GFX_idea_VOL_minister_Pavel_Belyayev"
			}
			army = {
				large = "gfx/leaders/VOL/Portrait_Vologda_Pavel_Belyayev.dds"
				small = "GFX_idea_VOL_minister_Pavel_Belyayev"
			}
		}

		advisor = {
			slot = security_minister
			idea_token = VOL_Pavel_Belyayev_sec
			allowed = { original_tag = VOL }
			traits = {
				security_minister
				sec_crime_fighter
			}
			ledger = civilian
			cost = -1
			removal_cost = -1
			ai_will_do = { factor = 0 }
		}

		corps_commander = {
			traits = {
				infantry_officer
				trait_engineer
			}
			skill = 2
			attack_skill = 1
			defense_skill = 1
			planning_skill = 3
			logistics_skill = 2
			legacy_id = 3422
		}
	}

	# Commanders
	VOL_Mikhail_Vlasov = {
		name = VOL_Mikhail_Vlasov
		portraits = {
			army = {
				large = "gfx/leaders/VOL/Portrait_Vologda_Mikhail_Vlasov.dds"
				#small = "GFX_idea_VOL_minister_Mikhail_Vlasov"
			}
		}

		corps_commander = {
			traits = { bearer_of_artillery panzer_leader winter_specialist }
			skill = 2
			attack_skill = 3
			defense_skill = 2
			planning_skill = 1
			logistics_skill = 1
			legacy_id = 3423
		}
	}

	VOL_Peter_Kapustin = {
		name = VOL_Peter_Kapustin
		portraits = {
			army = {
				large = "gfx/leaders/VOL/Portrait_Vologda_Peter_Kapustin.dds"
				#small = "GFX_idea_VOL_minister_Peter_Kapustin"
			}
		}

		corps_commander = {
			traits = { old_guard trickster expert_improviser }
			skill = 2
			attack_skill = 2
			defense_skill = 2
			planning_skill = 1
			logistics_skill = 2
			legacy_id = 3424
		}
	}

	VOL_Valentin_Bakhalov = {
		name = VOL_Valentin_Bakhalov
		portraits = {
			army = {
				large = "gfx/leaders/VOL/Portrait_Vologda_Valentin_Bakhalov.dds"
				#small = "GFX_idea_VOL_minister_Valentin_Bakhalov"
			}
		}

		corps_commander = {
			traits = { trait_engineer winter_specialist }
			skill = 2
			attack_skill = 1
			defense_skill = 2
			planning_skill = 2
			logistics_skill = 2
			legacy_id = 3425
		}
	}
	
}
