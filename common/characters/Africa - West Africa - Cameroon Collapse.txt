characters = {
	# Biafra
	BFR_Odumegwu_Ojukwu = {
		name = BFR_Odumegwu_Ojukwu
		portraits = {
			civilian = {
				large = "gfx/leaders/BFR/Portrait_Biafra_Chukwuemeka_Odumegwu_Ojukwu.dds"
				#small = ""
			}
			army = {
				large = "gfx/leaders/BFR/Portrait_Biafra_Chukwuemeka_Odumegwu_Ojukwu.dds"
				#small = ""
			}
		}
		field_marshal  = {
			traits = { media_personality inspirational_leader organizer }
			skill = 3
			attack_skill = 3
			defense_skill = 4
			planning_skill = 2
			logistics_skill = 4
		}
		country_leader = {
			desc = "POLITICS_OJUKWU_DESC"
			expire = "2000.7.7"
			ideology = despotism_military_junta_subtype
			traits = { }
			id = -1
		}
	}
}