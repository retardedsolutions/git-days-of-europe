characters = {
	ARC_David_Dragunsky = {
		name = ARC_David_Dragunsky
		portraits = {
			civilian = {
				large = "gfx/leaders/ARC/Portrait_ARC_David_Dragunsky.dds"
			}
		}
		country_leader = {
			desc = "POLITICS_DAVID_DRAGUNSKY_DESC"
			ideology = authoritarian_democracy_provisional_government_subtype
			expire = "1992.10.02"
			id = -1
		}
	}
}