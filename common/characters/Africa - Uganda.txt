characters = {
    UGD_mutesa_ii = {
        name = UGD_mutesa_ii
        portraits = {
            civilian = {
                large="gfx/leaders/UGD/Portrait_Uganda_Mutesa_II.dds"
            }
        }
        country_leader = {
            desc="POLITICS_MUTESA_II_DESC"
			ideology=despotism_absolute_monarchy_subtype
			traits={ }
			expire="1999.1.1.1"
			id=-1
        }
    }

    UGD_idi_amin_dada = {
        name = UGD_idi_amin_dada
        portraits = {
            civilian = {
                large="gfx/leaders/UGD/Portrait_Uganda_Idi_Amin_Dada.dds"
                small="GFX_idea_UGD_Idi_Amin_Dada_for"
                
            }
            army = {
                large="gfx/leaders/UGD/Portrait_Uganda_Idi_Amin_Dada.dds"
                small="GFX_idea_UGD_Idi_Amin_Dada_for"
            }
        }
        country_leader = {
            desc="POLITICS_MUTESA_II_DESC"
			ideology=ultranationalism_ultramilitarism_subtype
			traits={ }
			expire="1999.1.1.1"
			id=-1
        }
        field_marshal={
			traits={  }
			skill = 2
            attack_skill = 3
            defense_skill = 1
            planning_skill = 1
            logistics_skill = 2
		}
        advisor = {
			slot = foreign_minister
			idea_token = UGD_Idi_Amin_Dada_for
			allowed = { original_tag = UGD }
			traits = {
				foreign_minister
				for_warmonger
			}
			ledger = civilian
			cost = -1
			removal_cost = -1
			ai_will_do = { factor = 0 }
		}
    }

    UGD_juma_butabika = {
        name = UGD_juma_butabika
        portraits = {
            civilian = {
                small="GFX_idea_UGD_Juma_Butabika_eco"
            }
            army = {
                large="gfx/leaders/UGD/Portrait_Uganda_Juma_Butabika.dds"
                small="GFX_idea_UGD_Juma_Butabika_eco"
            }
        }
        corps_commander={
			traits = {  }
            skill = 2
            attack_skill = 2
            defense_skill = 1
            planning_skill = 2
            logistics_skill = 2
		}
        advisor = {
			slot = economy_minister
			idea_token = UGD_Juma_Butabika_eco
			allowed = { original_tag = UGD }
			traits = {
				economy_minister
				eco_corrupt_kleptocrat
			}
			ledger = civilian
			cost = -1
			removal_cost = -1
			ai_will_do = { factor = 0 }
		}
    }

    UGD_mustafa_adrisi = {
        name = UGD_mustafa_adrisi
        portraits = {
            civilian = {
                small="GFX_idea_UGD_Mustafa_Adrisi_sec"
            }
            army = {
                large="gfx/leaders/UGD/Portrait_Uganda_Mustafa_Adrisi.dds"
                small="GFX_idea_UGD_Mustafa_Adrisi_sec"
            }
        }
        corps_commander={
			traits = {  }
            skill = 2
            attack_skill = 1
            defense_skill = 2
            planning_skill = 2
            logistics_skill = 2
		}
        advisor = {
			slot = security_minister
			idea_token = UGD_Juma_Butabika_eco
			allowed = { original_tag = UGD }
			traits = {
				security_minister
				sec_petty_warlord
			}
			ledger = civilian
			cost = -1
			removal_cost = -1
			ai_will_do = { factor = 0 }
		}
    }

    UGD_isaac_maliyamungu = {
        name = UGD_isaac_maliyamungu
        portraits = {
            civilian = {
                small="GFX_idea_UGD_Isaac_Maliyamungu_hog"
            }
        }
        corps_commander={
			traits = {  }
            skill = 2
            attack_skill = 1
            defense_skill = 2
            planning_skill = 2
            logistics_skill = 2
		}
        advisor = {
			slot = head_of_government
			idea_token = UGD_Isaac_Maliyamungu_hog
			allowed = { original_tag = UGD }
			traits = {
				head_of_government
				hog_local_tyrant
			}
			ledger = civilian
			cost = -1
			removal_cost = -1
			ai_will_do = { factor = 0 }
		}
    }
    UGD_Tito_Okello = {
        name = UGD_Tito_Okello
        portraits = {
            civilian = {
                large= "gfx/leaders/UGD/Portrait_Uganda_Tito_Okello.dds"
            }
        }
        country_leader = {
			desc = "POLITICS_TITO_OKELLO_DESC"
			expire = "1999.1.23"
			ideology = fascism_revolutionary_nationalism_subtype
			traits = { }
            id = -1
		}
    }
}