is_existing_german_contender = {
	OR = {
		tag = SGR
		tag = GGR
		tag = BGR
		tag = HGR
		tag = RNW
		tag = GER
	}
}

GER_GCW_Heydrich_Indisposed = {
	GER = {
		OR = {
			has_country_flag = SS_heydrich_dead
			has_country_flag = SS_heydrich_in_prison
		}
	}
}

### State Ownership ###
is_BGR_state = {
	OR = {
		state = 4
		state = 50
		state = 52
		state = 53
		state = 54
		state = 60
		state = 65
		state = 69
		state = 72
		state = 152
		state = 153
		state = 770
		state = 1025
	}
}

is_GGR_state = {
	OR = {
		state = 58
		state = 61
		state = 62
		state = 63
		state = 66
		state = 67
		state = 68
		state = 74
		state = 86
		state = 87
		state = 188
		state = 774
		state = 775

		AND = {
			GER_GCW_Heydrich_Indisposed = yes
			OR = {
				state = 5
				state = 85
				state = 97
				state = 98
			}
		}
	}
}

is_SGR_state = {
	OR = {
		state = 51
		state = 55
		state = 56
		state = 57
		state = 59
		state = 775
		state = 1373

		AND = {
			GER_GCW_Heydrich_Indisposed = yes
			OR = {
				state = 8
				state = 28
				state = 42
			}
		}
	}
}

is_HGR_state = {
	NOT = { GER_GCW_Heydrich_Indisposed = yes }
	OR = {
		state = 5
		state = 8
		state = 28
		state = 42
		state = 85
		state = 97
		state = 98
	}
}

GCW_legitimacy_more_than_or_equals_5 = {
	if = {
		limit = {
			has_country_flag = bormann_for_germany
		}
		check_variable = { GER_Bormann_Influence > 4 }
	}
	else_if = {
		limit = {
			has_country_flag = goring_for_germany
		}
		check_variable = { GER_Goring_Influence > 4 }
	}
	else_if = {
		limit = {
			has_country_flag = speer_for_germany
		}
		check_variable = { GER_Speer_Influence > 4 }
	}
	else_if = {
		limit = {
			has_country_flag = heydrich_for_germany
		}
		check_variable = { GER_Heydrich_Influence > 4 }
	}
}

GCW_legitimacy_more_than_or_equals_10 = {
	if = {
		limit = {
			has_country_flag = bormann_for_germany
		}
		check_variable = { GER_Bormann_Influence > 9 }
	}
	else_if = {
		limit = {
			has_country_flag = goring_for_germany
		}
		check_variable = { GER_Goring_Influence > 9 }
	}
	else_if = {
		limit = {
			has_country_flag = speer_for_germany
		}
		check_variable = { GER_Speer_Influence > 9 }
	}
	else_if = {
		limit = {
			has_country_flag = heydrich_for_germany
		}
		check_variable = { GER_Heydrich_Influence > 9 }
	}
}