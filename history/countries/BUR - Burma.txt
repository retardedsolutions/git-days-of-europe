﻿capital = 288

# Vacant
recruit_character = BUR_Generic_Vacant


recruit_character = BUR_ba_maw
recruit_character = BUR_ne_win
recruit_character = BUR_aung_san
recruit_character = BUR_ba_swe
recruit_character = BUR_thakin_soe
recruit_character = BUR_kyaw_nyein
recruit_character = BUR_u_saw
recruit_character = BUR_let_ya
recruit_character = BUR_kyaw_zaw
recruit_character = BUR_hmu_aung

fill_empty_minister_nochecks = yes # Please remove if you're adding ministers.

oob = "BUR_1962"


set_technology = {
	basic_train = 1

	##INFANTRY##
 	infantry_weapons_1 = 1
	infantry_weapons_2 = 1
	infantry_weapons_improvements_1 = 1
	infantry_weapons_improvements_2 = 1
	infantry_weapons_improvements_3 = 1
	infantry_weapons_improvements_4 = 1
 	basic_infantry_equipment = 1
	support_weapons = 1
	infantry_at = 1
	infantry_at2 = 1
	motorized_infantry = 1
	#infantry_aa = 1
	#infantry_aa2 = 1
	##SUPPORT##
 	tech_support = 1
 	tech_engineers = 1
	tech_engineers_flamethrowers_1 = 1
	tech_recon = 1
	tech_recon2 = 1
	##ARTY##
 	artillery4 = 1
	antiair5 = 1
	##ARMOR##
	advanced_light_tank = 1
	advanced_medium_tank = 1
	mechanised_infantry3 = 1

	APC_1950 = 1
	APC_1950_basic_turret = 1
	#APC_1950_basic_engine = 1

	IFV_1950 = 1
	
	MBT_1950 = 1

	##AIRCRAFT##
	early_fighter = 1
	fighter_1945 = 1
	interceptor_1945 = 1
	CAS_1945 = 1
	##HEAVY AIRCRAFT##
	strategic_bomber1 = 1
	heavy_air_1945 = 1
	tac_bomber1 = 1
	tac_air_1945 = 1
	transport_air1 = 1
	transport_air_1945 = 1
	##ENGINEERING##
	electronic_mechanical_engineering = 1
	radio = 1
	mechanical_computing = 1
	computing_machine = 1
	basic_encryption = 1
	basic_decryption = 1
	##INDUSTRY##
	ww2_line_production = 1
	batch_production_1 = 1
	energy_technology_1 = 1
    social_construction_1 = 1
	military_construction_1 = 1
}

set_politics = {	
	ruling_party = despotism
	last_election = "1961.1.20"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    communist = 2
    ultranationalism = 0
    socialist = 2
    social_democracy = 1
    liberal_democracy = 0
    conservative_democracy = 0
    authoritarian_democracy = 31
    despotism = 45
    fascism = 13
    national_socialism = 6
    
}

add_ideas = {
	##Starting##
	Sphere_Economic_Dependent
	BUR_Minority_Unrest_dummy
	BUR_Politicized_Society
	BUR_Two_Faced_Burma
	##MINISTERS###
	
	##POLITICAL LAWS##
	tno_political_parties_one_party_state
	tno_religious_rights_state_religion
	tno_trade_unions_illegal
	tno_immigration_closed_borders
	tno_slavery_outlawed
	tno_public_meetings_outlawed
	tno_security_police
	##MILITARY LAWS###
	tno_conscription_volunteer_only
	tno_women_banned
	tno_military_supervision_kill_em_all
	tno_training_minimal_training
	tno_racial_integration_segregated_regiments
	##SOCIAL LAWS##
	tno_safety_minimal_regulations
	tno_health_care_low_income_protections
	tno_pollution_no_controls
	tno_education_public_education
	tno_penal_system_penal_labor
	tno_lgbt_rights_lgbt_outlawed
	##ECONOMIC LAWS##
	tno_trade_laws_limited_exports
	tno_income_taxation_elite_tax_exemptions
	tno_minimum_wage_trinket_minimum_wage
	tno_max_workhours_unlimited_work_day
		 
     tno_army_professionalism_disgruntled_veterans
     tno_industrial_expertise_nascent
     tno_industrial_equipment_power_tools
     tno_agriculture_basic_mechanized
     tno_research_facilities_outdated
     tno_academic_base_basic_literacy
	 tno_admin_efficiency_deficient_administrative_systems
}
