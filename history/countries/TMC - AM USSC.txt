﻿capital = 637

# Vacant
recruit_character = TMC_Generic_Vacant
fill_empty_minister_nochecks = yes # Please remove if you're adding ministers.
recruit_character = TMC_Nikolai_Artamonov

set_country_flag = is_russian_nation

oob = "TMC_1962"

###Playable
set_country_flag = tno_playable_country

add_ideas = {
### POLITICAL LAWS ###
	tno_political_parties_one_party_state
	tno_religious_rights_state_religion
	tno_trade_unions_illegal
	tno_immigration_open_immigration
	tno_slavery_outlawed
	tno_public_meetings_regulated
	tno_press_rights_censored_press
	tno_vote_franchise_no_voting
	tno_refugees_vetted_entry

	### MILITARY LAWS ###
	tno_conscription_two_year_draft
	tno_women_banned
	tno_military_supervision_military_policing
	tno_training_basic_training
	tno_racial_integration_segregated_regiments
	tno_draft_exemptions_none

	### SOCIAL LAWS ###
	tno_safety_minimal_regulations
	tno_health_care_support_for_emergencies
	tno_pollution_few_regulations
	tno_education_elite_only
	tno_penal_system_penal_labor
	tno_lgbt_rights_lgbt_outlawed
	tno_gender_rights_traditional_roles
	tno_minorities_segregation
	tno_security_police

	### ECONOMIC LAWS ###
	tno_trade_laws_limited_exports
	tno_income_taxation_elite_tax_exemptions
	tno_minimum_wage_trinket_minimum_wage
	tno_max_workhours_12_hour_work_day
	tno_child_labor_illegal
	tno_pensions_trinket_pensions
	tno_unemployment_trinket_subsidies

	### SOCIETAL DEVELOPMENT ###
	
    tno_army_professionalism_disgruntled_veterans
    tno_industrial_expertise_nascent
    tno_industrial_equipment_power_tools
    tno_agriculture_basic_mechanized
    tno_research_facilities_outdated
    tno_academic_base_basic_literacy
	tno_admin_efficiency_deficient_administrative_systems
}

set_technology = {
	basic_train = 1

	##INFANTRY##
	infantry_weapons_1 = 1
	infantry_weapons_2 = 1
	infantry_weapons_improvements_1 = 1
	infantry_weapons_improvements_2 = 1
	infantry_weapons_improvements_3 = 1
	infantry_weapons_improvements_4 = 1
	basic_infantry_equipment = 1
	support_weapons = 1
	infantry_at = 1
	infantry_at2 = 1
	motorized_infantry = 1
	#infantry_aa = 1
	#infantry_aa2 = 1
	##SUPPORT##
	tech_support = 1
	tech_engineers = 1
	tech_engineers_flamethrowers_1 = 1
	tech_military_police = 1
	tech_recon = 1
	tech_recon2 = 1
	##ARTY##
	
	##ARMOR##
	

	##AIRCRAFT##
	early_fighter = 1
	fighter_1945 = 1
	interceptor_1945 = 1
	CAS_1945 = 1
	##HEAVY AIRCRAFT##
	strategic_bomber1 = 1
	heavy_air_1945 = 1
	tac_bomber1 = 1
	tac_air_1945 = 1
	transport_air1 = 1
	transport_air_1945 = 1
	##HELICOPTERS##
	helicopter_engines = 1
	very_early_helicopter = 1
	early_helicopter = 1
	##ENGINEERING##
	electronic_mechanical_engineering = 1
	radio = 1
	mechanical_computing = 1
	computing_machine = 1
	basic_encryption = 1
	basic_decryption = 1
	##INDUSTRY##
	ww2_line_production = 1
	batch_production_1 = 1
	building_construction_1 = 1
	resource_extraction_methods = 1
	excavation0 = 1
	energy_technology_1 = 1
	social_construction_1 = 1
	military_construction_1 = 1
}

set_politics = {	
	ruling_party = despotism
	last_election = "1933.5.17"
	election_frequency = 60
	elections_allowed = no
}

set_popularities = {
    communist = 0
    ultranationalism = 0
    socialist = 0
    social_democracy = 0
    liberal_democracy = 0

    conservative_democracy = 0
    authoritarian_democracy = 25
    despotism = 75
    fascism = 0
    
}

TNO_startup_nation = yes 
