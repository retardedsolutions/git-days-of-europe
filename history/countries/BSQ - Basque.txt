﻿capital = 993

# Vacant
recruit_character = BSQ_Generic_Vacant



set_country_flag = is_iberian_nation

set_war_support = 0.8
set_stability = 0.5

###Playable
set_country_flag = tno_playable_country

oob = "BSQ_1962"

set_politics = {	
	ruling_party = authoritarian_democracy
	last_election = "1962.1.1"
	election_frequency = 36
	elections_allowed = yes
}


set_popularities = {
    communist = 10
    ultranationalism = 0
    socialist = 22
    social_democracy = 14
    liberal_democracy = 4

    conservative_democracy = 27
    authoritarian_democracy = 18
    despotism = 3
    fascism = 2
    
}

recruit_character = BSQ_Jesus_Leizaola
recruit_character = BSQ_Jose_Miguel_Benaran
recruit_character = BSQ_Juan_Ajuriagerra
recruit_character = BSQ_Domingo_Iturbide
recruit_character = BSQ_Txabi_Etxebarrieta
recruit_character = BSQ_Angel_Otaegui
recruit_character = BSQ_Xabier_Zumalde

recruit_character = BSQ_Jose_Quesada
recruit_character = BSQ_Telesforo_Monzon
recruit_character = BSQ_Xabier_Etxebarrieta
recruit_character = BSQ_Jose_Maria_Benegas
recruit_character = BSQ_Julen_Gimon
recruit_character = BSQ_Jesus_Maria_Leizaola
recruit_character = BSQ_Ignacio_Iturbide
recruit_character = BSQ_Mariano_Sanchez

recruit_character = BSQ_Antonio_Ibanez
recruit_character = BSQ_Carlos_Villar
recruit_character = BSQ_Joseba_Elosegui
recruit_character = BSQ_Pablo_Beldarrain
recruit_character = BSQ_Juan_Castro
recruit_character = BSQ_Rafael_Menchaca
recruit_character = BSQ_Alejo_Bilbao
add_ideas = {
	IBR_regional_faction_buff
	#cabinet 
	BSQ_Jesus_Leizaola_hog
	BSQ_Txabi_Etxebarrieta_for
	BSQ_Juan_Ajuriagerra_eco
	BSQ_Jose_Miguel_Benaran_sec
	#political#
	tno_political_parties_multiparty_system
	tno_religious_rights_pluralism
	tno_trade_unions_all_allowed
	tno_immigration_quota_immigration
	tno_slavery_outlawed
	tno_public_meetings_allowed
	tno_press_rights_free_press
	tno_vote_franchise_universal
	tno_refugees_skilled
	#military#
	tno_conscription_service_by_requirement
	tno_women_military_assistance
	tno_military_supervision_military_policing
	tno_training_basic_training
	tno_racial_integration_integrated_military
	tno_draft_exemptions_none
	#economic#
	tno_trade_laws_export_focus
	tno_income_taxation_high_income_weighted
	tno_minimum_wage_acceptable_minimum
	tno_max_workhours_10_hour_work_day
	tno_child_labor_illegal
	tno_pensions_acceptable_pensions
	tno_unemployment_generous_subsidies
	#social#
	tno_safety_acceptable_regulations
	tno_health_care_low_income_protections
	tno_pollution_some_regulations
	tno_education_public_education
	tno_penal_system_incarceration
	tno_lgbt_rights_lgbt_outlawed
	tno_gender_rights_women_in_the_workplace
	tno_minorities_equal_rights
	tno_security_police
	#socdev
	tno_admin_efficiency_deficient_administrative_systems
	
	tno_army_professionalism_political_interference
	tno_industrial_expertise_experienced
	tno_industrial_equipment_factory_complexes
	tno_agriculture_mass_mechanized
	tno_research_facilities_outdated
	tno_academic_base_primary_schooling
}
if = {
	limit = { has_dlc = "Man the Guns" }
	create_equipment_variant = {
		name = "Clase Liniers"
		type = ship_hull_light_3
		name_group = GER_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_2
			fixed_ship_fire_control_system_slot = ship_fire_control_system_1
			fixed_ship_radar_slot = ship_radar_1
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = ship_light_battery_1
			rear_1_custom_slot = ship_anti_air_2
    	}
	}
	create_equipment_variant = {
		name = "Clase Audaz"
		type = ship_hull_light_2
		name_group = GER_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_2
			fixed_ship_fire_control_system_slot = ship_fire_control_system_1
			fixed_ship_radar_slot = ship_radar_1
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = ship_light_battery_1
			rear_1_custom_slot = ship_anti_air_2
    	}
	}
	create_equipment_variant = {
		name = "Clase Atrevida"
		type = ship_hull_very_light_3
		name_group = GER_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_2
			fixed_ship_fire_control_system_slot = ship_fire_control_system_1
			fixed_ship_radar_slot = ship_radar_1
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = ship_light_battery_1
			rear_1_custom_slot = ship_anti_air_2
    	}
	}
	create_equipment_variant = {
		name = "Clase Descubierta"
		type = ship_hull_very_light_2
		name_group = GER_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_2
			fixed_ship_fire_control_system_slot = ship_fire_control_system_1
			fixed_ship_radar_slot = ship_radar_1
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = ship_light_battery_1
			rear_1_custom_slot = ship_anti_air_2
    	}
	}
	create_equipment_variant = {
		name = "Clase Vicente Yáñez Pinzón"
		type = ship_hull_more_light_3
		name_group = GER_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_2
			fixed_ship_fire_control_system_slot = ship_fire_control_system_1
			fixed_ship_radar_slot = ship_radar_1
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = ship_light_battery_1
			rear_1_custom_slot = ship_anti_air_2
    	}
	}
	create_equipment_variant = {
		name = "Clase Pizarro"
		type = ship_hull_more_light_2
		name_group = GER_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_2
			fixed_ship_fire_control_system_slot = ship_fire_control_system_1
			fixed_ship_radar_slot = ship_radar_1
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = ship_light_battery_1
			rear_1_custom_slot = ship_anti_air_2
    	}
	}
	create_equipment_variant = {
		name = "Clase Pizarro"
		type = ship_hull_more_light_1
		name_group = GER_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_2
			fixed_ship_fire_control_system_slot = ship_fire_control_system_1
			fixed_ship_radar_slot = ship_radar_1
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = ship_light_battery_1
			rear_1_custom_slot = ship_anti_air_2
    	}
	}
	create_equipment_variant = {
		name = "Clase D"
		type = ship_hull_submarine_2
		name_group = GER_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			rear_1_custom_slot = ship_anti_air_2
    	}
	}
	create_equipment_variant = {
		name = "S-class"
		type = ship_hull_submarine_1
		name_group = GER_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			rear_1_custom_slot = ship_anti_air_2
    	}
	}
}
if = {
	limit = {
		NOT = { 
			has_dlc = "Man the Guns" 
		}
	}
	create_equipment_variant = {
		name = "Clase Pizarro"
		type = frigate_2
		parent_version = 0
	}
	create_equipment_variant = {
		name = "Clase Vicente Yáñez Pinzón"
		type = frigate_3
		parent_version = 0
	}
	create_equipment_variant = {
		name = "S-class"
		type = submarine_1
		parent_version = 0
	}
	create_equipment_variant = {
		name = "Clase D"
		type = submarine_2
		parent_version = 0
	}
	create_equipment_variant = {
		name = "Clase Liniers"
		type = destroyer_3
		parent_version = 0
	}
	create_equipment_variant = {
		name = "Clase Audaz"
		type = destroyer_2
		parent_version = 0
	}
}
add_opinion_modifier = { target = BSQ modifier = IBR_Iberian_Wars_Contender }
add_opinion_modifier = { target = PRT modifier = IBR_Iberian_Wars_Contender }
add_opinion_modifier = { target = SPS modifier = IBR_Iberian_Wars_Contender }
add_opinion_modifier = { target = SPA modifier = IBR_Iberian_Wars_Contender }
add_opinion_modifier = { target = SPR modifier = IBR_Iberian_Wars_Contender }
add_opinion_modifier = { target = CNT modifier = IBR_Iberian_Wars_Contender }
add_opinion_modifier = { target = CTL modifier = IBR_Iberian_Wars_Contender }
add_opinion_modifier = { target = FSR modifier = IBR_Iberian_Wars_Contender }
add_opinion_modifier = { target = FSR modifier = IBR_NRF_NuffSaid }
add_opinion_modifier = { target = POR modifier = IBR_Iberian_Wars_Contender }
add_opinion_modifier = { target = RPP modifier = IBR_Iberian_Wars_Contender }
add_opinion_modifier = { target = PPR modifier = IBR_Iberian_Wars_Contender }
add_opinion_modifier = { target = GAL modifier = IBR_Iberian_Wars_Contender }
add_opinion_modifier = { target = GNS modifier = IBR_Iberian_Wars_Contender }
add_opinion_modifier = { target = ETA modifier = IBR_Iberian_Wars_Contender }
add_opinion_modifier = { target = MOR modifier = IBR_Iberian_Wars_Contender }
add_opinion_modifier = { target = WAI modifier = IBR_Iberian_Wars_Contender }
add_opinion_modifier = { target = IBR modifier = IBR_Iberian_Wars_Contender }
	
add_opinion_modifier = { target = BSQ modifier = IBR_Iberian_Wars_Contender_2 }
add_opinion_modifier = { target = PRT modifier = IBR_Iberian_Wars_Contender_2 }
add_opinion_modifier = { target = SPS modifier = IBR_Iberian_Wars_Contender_2 }
add_opinion_modifier = { target = SPA modifier = IBR_Iberian_Wars_Contender_2 }
add_opinion_modifier = { target = SPR modifier = IBR_Iberian_Wars_Contender_2 }
add_opinion_modifier = { target = CNT modifier = IBR_Iberian_Wars_Contender_2 }
add_opinion_modifier = { target = CTL modifier = IBR_Iberian_Wars_Contender_2 }
add_opinion_modifier = { target = FSR modifier = IBR_Iberian_Wars_Contender_2 }
add_opinion_modifier = { target = POR modifier = IBR_Iberian_Wars_Contender_2 }
add_opinion_modifier = { target = RPP modifier = IBR_Iberian_Wars_Contender_2 }
add_opinion_modifier = { target = PPR modifier = IBR_Iberian_Wars_Contender_2 }
add_opinion_modifier = { target = GAL modifier = IBR_Iberian_Wars_Contender_2 }
add_opinion_modifier = { target = GNS modifier = IBR_Iberian_Wars_Contender_2 }
add_opinion_modifier = { target = ETA modifier = IBR_Iberian_Wars_Contender_2 }
add_opinion_modifier = { target = MOR modifier = IBR_Iberian_Wars_Contender_2 }
add_opinion_modifier = { target = WAI modifier = IBR_Iberian_Wars_Contender_2 }
add_opinion_modifier = { target = IBR modifier = IBR_Iberian_Wars_Contender_2 }

TNO_startup_nation = yes 

