﻿capital = 371

# Vacant
recruit_character = AMK_Generic_Vacant
recruit_character = AMK_hansjoachim_hajo_herrmann
fill_empty_minister_nochecks = yes # Please remove if you're adding ministers.



add_ideas = {
### POLITICAL LAWS ###
	tno_political_parties_one_party_state
	tno_religious_rights_state_atheism
	tno_trade_unions_illegal
	tno_immigration_open_immigration
	tno_slavery_outlawed
	tno_public_meetings_regulated
	tno_press_rights_censored_press
	tno_vote_franchise_no_voting
	tno_refugees_banned

	### MILITARY LAWS ###
	tno_conscription_two_year_draft
	tno_women_banned
	tno_military_supervision_military_policing
	tno_training_basic_training
	tno_racial_integration_segregated_regiments
	tno_draft_exemptions_none

	### SOCIAL LAWS ###
	tno_safety_minimal_regulations
	tno_health_care_support_for_emergencies
	tno_pollution_few_regulations
	tno_education_elite_only
	tno_penal_system_penal_labor
	tno_lgbt_rights_lgbt_outlawed
	tno_gender_rights_traditional_roles
	tno_minorities_segregation
	tno_security_police

	### ECONOMIC LAWS ###
	tno_trade_laws_limited_exports
	tno_income_taxation_elite_tax_exemptions
	tno_minimum_wage_trinket_minimum_wage
	tno_max_workhours_12_hour_work_day
	tno_child_labor_illegal
	tno_pensions_trinket_pensions
	tno_unemployment_trinket_subsidies

	### SOCIETAL DEVELOPMENT ###
	
	tno_army_professionalism_political_interference
	tno_industrial_expertise_nascent
	tno_industrial_equipment_power_tools
	tno_admin_efficiency_deficient_administrative_systems
	tno_agriculture_basic_mechanized
	tno_research_facilities_outdated
	tno_academic_base_basic_literacy
}

set_politics = {
 	ruling_party = national_socialism
 	last_election = "1933.3.5"
 	election_frequency = 48
 	elections_allowed = no

}

set_popularities = {
    authoritarian_democracy = 20
    national_socialism = 80
}

AMK_hansjoachim_hajo_herrmann = {
	promote_character = { ideology = national_socialism_stratocratic_nazism_subtype }
}

#econtype
set_variable = { TNO_economy_type = 2 }
set_variable = { TNO_economy_subtype = 5 }

TNO_startup_nation = yes 
