﻿capital = 119

# Vacant
recruit_character = ULS_Generic_Vacant




set_research_slots = 1
set_stability = -0.12
set_war_support = 0.89

set_technology = {
	basic_train = 1

	infantry_weapons_1 = 1
	infantry_weapons_2 = 1
	infantry_weapons_improvements_1 = 1
	infantry_weapons_improvements_2 = 1
	basic_infantry_equipment = 1
	support_weapons = 1
	infantry_at = 1

	motorized_infantry = 1
	tech_mountaineers = 1

	tech_support = 1
	tech_engineers = 1
	tech_engineers_flamethrowers_1 = 1
	tech_recon = 1
	tech_military_police = 1
	tech_maintenance_company = 1
	tech_field_hospital = 1
	tech_logistics_company = 1
	tech_signal_company = 1

	advanced_light_tank = 1
	advanced_medium_tank = 1
	mechanised_infantry3 = 1

	antiair5 = 1
	antiair_1950 = 1
	SPAA_1950 = 1
	artillery4 = 1
	artillery_1950 = 1
	artillery_1950_soft_attack = 1
	SPART_1950 = 1
	SPART_1950_soft_attack = 1

	##NAVAL##
	ww2_destroyer = 1
	ww2_cruiser = 1
	basic_cruiser = 1
	ww2_battleship = 1
	ww2_submarine = 1
	basic_submarine = 1
	improved_submarine = 1
	transport = 1

	##ENGINEERING##
	electronic_mechanical_engineering = 1
	radio = 1
	radio_detection = 1
	decimetric_radar = 1
	centimetric_radar = 1
	mechanical_computing = 1
	computing_machine = 1
	basic_encryption = 1
	basic_decryption = 1
	improved_computing_machine = 1
	improved_encryption = 1
	improved_decryption = 1
	advanced_computing_machine = 1
	advanced_encryption = 1

	##INDUSTRY##
	ww2_line_production = 1
	building_construction_1 = 1
	energy_technology_1 = 1
	social_construction_1 = 1
	military_construction_1 = 1
}

recruit_character = ULS_Neil_Blaney
recruit_character = ULS_James_Kelly
recruit_character = ULS_Ian_Paisley
recruit_character = ULS_Gerry_Fitt
recruit_character = ULS_Seamus_Twomey
recruit_character = ULS_Robin_Chichester_Clark
recruit_character = ULS_Phelim_ONeill
recruit_character = ULS_REDACTED
recruit_character = ULS_Unknown_com
recruit_character = ULS_Unknown_ultranat
recruit_character = ULS_Terence_ONeill
recruit_character = ULS_Eddie_MacAteer
recruit_character = ULS_Albert_Kennedy
recruit_character = ULS_Austin_Currie


add_ideas = {

	### FACTION ###

	Pakt_Bundnispartner
	
	### NATIONAL SPIRITS ###

	ULS_The_Strangest_Machine
	ULS_Rifles_Round_Every_Corner

	### POLITIAL ###

	tno_political_parties_controlled_opposition
	tno_religious_rights_state_religion
	tno_trade_unions_illegal
	tno_immigration_closed_borders
	tno_slavery_outlawed
	tno_public_meetings_regulated
	tno_press_rights_censored_press
	tno_vote_franchise_registered_voting
	tno_refugees_banned
	
	### MILITARY ###

	tno_conscription_service_by_requirement
	tno_women_banned
	tno_military_supervision_military_policing
	tno_training_minimal_training
	tno_racial_integration_none
	tno_draft_exemptions_religious_deferment

	### ECOMOMIC ###

	tno_trade_laws_closed_economy
	tno_income_taxation_elite_tax_exemptions
	tno_minimum_wage_trinket_minimum_wage
	tno_max_workhours_12_hour_work_day
	tno_child_labor_illegal
	tno_pensions_trinket_pensions
	tno_unemployment_no_subsidies

	### SOCIAL ###

	tno_safety_minimal_regulations
	tno_health_care_support_for_emergencies
	tno_pollution_no_controls
	tno_education_public_education
	tno_penal_system_penal_labor
	tno_lgbt_rights_lgbt_outlawed
	tno_gender_rights_women_in_the_workplace
	tno_minorities_segregation
	tno_security_wire_tapping
	
	### MINISTERS ###

	ULS_Eddie_MacAteer_hog
	ULS_Albert_Kennedy_sec
	ULS_Austin_Currie_eco
	ULS_Gerry_Fitt_for

	### ECONOMY (WHAT A FUCKING JOKE) ###

	##construction_level_3

	### SOCIETAL DEVELOPMENT ###

	tno_academic_base_secondary_schooling
	tno_research_facilities_outdated
	tno_agriculture_basic_mechanized
	tno_industrial_equipment_manufacturing_lines
	tno_industrial_expertise_incompetent
	tno_army_professionalism_reluctant_conscripts
	
}

set_politics = {	
	ruling_party = despotism
	last_election = "1933.1.24"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    communist = 10
    national_socialism = 0
    ultranationalism = 10
    socialist = 0
    social_democracy = 5
    liberal_democracy = 0

    conservative_democracy = 0
    authoritarian_democracy = 5
    despotism = 60
    fascism = 10
    
}
