﻿capital = 1539

recruit_character = AZW_Zodi_Ikhia
recruit_character = AZW_Intalla_ag_Attaher
recruit_character = AZW_Bey_ag_Akhamouk
recruit_character = AZW_Hamid_Algabid
recruit_character = AZW_Zeid_ag_Attaher
recruit_character = AZW_Alladi_ag_Alla
recruit_character = AZW_Hadj_Moussa_ag_Akhamouk

recruit_character = AZW_Generic_Vacant

#oob = "AZW_1962"

add_ideas = {
	WEST_AFRICA_idea_terror_bombing
	AZW_idea_tuareg_heritage
	AZW_idea_saharan_nomads

	AZW_Intalla_ag_Attaher_hog
	AZW_Bey_ag_Akhamouk_for
	AZW_Zodi_Ikhia_eco
	AZW_Intalla_ag_Attaher_sec
	### POLITICAL LAWS ###
	tno_political_parties_one_party_state
	tno_religious_rights_state_religion
	tno_trade_unions_illegal
	tno_immigration_open_immigration
	tno_slavery_outlawed
	tno_public_meetings_regulated
	tno_press_rights_censored_press
	tno_vote_franchise_no_voting
	tno_refugees_vetted_entry

	### MILITARY LAWS ###
	tno_conscription_two_year_draft
	tno_women_banned
	tno_military_supervision_military_policing
	tno_training_basic_training
	tno_racial_integration_segregated_regiments
	tno_draft_exemptions_none

	### SOCIAL LAWS ###
	tno_safety_minimal_regulations
	tno_health_care_support_for_emergencies
	tno_pollution_few_regulations
	tno_education_elite_only
	tno_penal_system_penal_labor
	tno_lgbt_rights_lgbt_outlawed
	tno_gender_rights_traditional_roles
	tno_minorities_segregation
	tno_security_police

	### ECONOMIC LAWS ###
	tno_trade_laws_limited_exports
	tno_income_taxation_elite_tax_exemptions
	tno_minimum_wage_trinket_minimum_wage
	tno_max_workhours_12_hour_work_day
	tno_child_labor_illegal
	tno_pensions_trinket_pensions
	tno_unemployment_trinket_subsidies

	### SOCIETAL DEVELOPMENT ###
	 
     tno_army_professionalism_disgruntled_veterans
     tno_industrial_expertise_nascent
     tno_industrial_equipment_power_tools
     tno_agriculture_basic_mechanized
     tno_research_facilities_outdated
     tno_academic_base_basic_literacy
	 tno_admin_efficiency_deficient_administrative_systems
}

set_politics = {	
	ruling_party = despotism
	last_election = "1961.1.1"
	election_frequency = 96
	elections_allowed = no
}

set_popularities = {
    communist = 0
    ultranationalism = 0
    socialist = 0
    social_democracy = 0
    liberal_democracy = 0

    conservative_democracy = 6
    authoritarian_democracy = 23
    despotism = 68
    fascism = 3
    
}

set_country_flag = TNO_eliminate_military_spending
