﻿capital = 37

# Vacant
recruit_character = DEN_Generic_Vacant

# Country Leaders
recruit_character = DEN_Werner_Best
recruit_character = DEN_Hermann_von_Hanneken
recruit_character = DEN_Knud_Borge_Martinsen
recruit_character = DEN_Carl_Popp_Madsen
recruit_character = DEN_Nils_Svenningsen
recruit_character = DEN_Erik_Eriksen
recruit_character = DEN_Jens_Otto_Krag
recruit_character = DEN_Occupational_Authorities
recruit_character = DEN_Gustav_Meissner

# Ministers
recruit_character = DEN_Georg_Ferdinand_Duckwitz
recruit_character = DEN_Axel_Gunnar_Larsen

# Generals and Admirals
recruit_character = DEN_Gunther_Pancke
recruit_character = DEN_Erhard_Qvistgaard
recruit_character = DEN_Jorgen_Hviid

oob = "DEN_1962"

add_ideas = { 
	#Faction
	Pakt_Marionettenstaat
	#Spirits
	DEN_permanent_secretaries
	DEN_agrarian_economy
	DEN_threat_from_silkeborg
	#Ministers
	DEN_Nils_Svenningsen_hog
	DEN_Axel_Gunnar_Larsen_eco
	DEN_Hermann_von_Hanneken_sec
	DEN_Georg_Ferdinand_Duckwitz_for
	### POLITICAL LAWS ###
	tno_political_parties_one_party_state
	tno_religious_rights_state_religion
	tno_trade_unions_illegal
	tno_immigration_open_immigration
	tno_slavery_outlawed
	tno_public_meetings_regulated
	tno_press_rights_censored_press
	tno_vote_franchise_no_voting
	tno_refugees_vetted_entry

	### MILITARY LAWS ###
	tno_conscription_two_year_draft
	tno_women_banned
	tno_military_supervision_military_policing
	tno_training_basic_training
	tno_racial_integration_segregated_regiments
	tno_draft_exemptions_none

	### SOCIAL LAWS ###
	tno_safety_minimal_regulations
	tno_health_care_support_for_emergencies
	tno_pollution_few_regulations
	tno_education_elite_only
	tno_penal_system_penal_labor
	tno_lgbt_rights_lgbt_outlawed
	tno_gender_rights_traditional_roles
	tno_minorities_segregation
	tno_security_police

	### ECONOMIC LAWS ###
	tno_trade_laws_limited_exports
	tno_income_taxation_elite_tax_exemptions
	tno_minimum_wage_trinket_minimum_wage
	tno_max_workhours_12_hour_work_day
	tno_child_labor_illegal
	tno_pensions_trinket_pensions
	tno_unemployment_trinket_subsidies

	### SOCIETAL DEVELOPMENT ###
	 
    tno_army_professionalism_disgruntled_veterans
    tno_industrial_expertise_nascent
    tno_industrial_equipment_power_tools
    tno_agriculture_basic_mechanized
    tno_research_facilities_outdated
    tno_academic_base_basic_literacy
	tno_admin_efficiency_deficient_administrative_systems
}

set_technology = {
	basic_train = 1

	##Infantry##
	infantry_weapons_1 = 1
	infantry_weapons_2 = 1
	infantry_weapons_improvements_1 = 1
	infantry_weapons_improvements_2 = 1
	infantry_weapons_improvements_3 = 1
	infantry_weapons_improvements_4 = 1
	infantry_weapons_3 = 1
	basic_infantry_equipment = 1
	support_weapons = 1
	support_weapons2 = 1
	night_vision_1 = 1
	infantry_at = 1
	infantry_at2 = 1
	infantry_at3 = 1
	motorized_infantry = 1
	motorized_infantry_1 = 1
	##Support Companies##
	tech_support = 1
	tech_engineers = 1
	tech_engineers2 = 1
	tech_recon = 1
	tech_recon2 = 1
	##Armor##
	mechanised_infantry3 = 1
	advanced_light_tank = 1
	advanced_medium_tank = 1
	##Artillery##
	antiair5 = 1
	antiair_1950 = 1
	artillery4 = 1
	artillery_1950 = 1
	##Land Doctrine##
	strategic_theorem = 1
}

set_politics = {	
	ruling_party = authoritarian_democracy
	last_election = "1935.10.22"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    communist = 0
    socialist = 0
    social_democracy = 12
    liberal_democracy = 7

    conservative_democracy = 5
    authoritarian_democracy = 33
    despotism = 8
    fascism = 12
    national_socialism = 23
    ultranationalism = 0
    
}

#econ types
set_variable = { TNO_economy_subtype = 5 }
set_variable = { TNO_economy_type = 2 }

if = {
	limit = { has_dlc = "Man the Guns" }
	set_naval_oob = DEN_mtg_navy_1962
	create_equipment_variant = {
		name = "H-Class"
		type = ship_hull_submarine_1
		name_group = GER_SS_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_engine_slot = sub_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_sub_1
			rear_1_custom_slot = ship_torpedo_sub_1
    	}
	}
	create_equipment_variant = {
		name = "Triton-Class"
		type = ship_hull_more_light_1
		name_group = GER_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_1
			fixed_ship_radar_slot = ship_sonar_1
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
    	}
	}
}

if = {
	limit = {
		not = { has_dlc = "Man the Guns" }
	}
	set_naval_oob = DEN_navy_1962
	create_equipment_variant = {
		name = "H-Class"
		type = submarine_1
		name_group = GER_SS_HISTORICAL
	}
	create_equipment_variant = {
		name = "Triton-Class"
		type = frigate_1
		name_group = GER_DD_HISTORICAL
	}
}
