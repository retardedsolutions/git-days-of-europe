﻿capital = 671 #Hanoi

# Vacant
recruit_character = VIN_Generic_Vacant



recruit_character = VIN_ho_chi_minh
recruit_character = VIN_bao_dai
recruit_character = VIN_ngo_dinh_diem
recruit_character = VIN_nguyen_cao_ky
recruit_character = VIN_truong_chinh

fill_empty_minister_nochecks = yes # Please remove if you're adding ministers.


VIN = {
 	set_cosmetic_tag = VIN_FIXED
}

oob = "VIN_1962"

set_politics = {
	ruling_party = despotism
	last_election = "1960.11.11"
	election_frequency = 36
	elections_allowed = no
}

set_popularities = {
    communist = 31
    authoritarian_democracy = 2
    despotism = 67
}

set_technology = {
	basic_train = 1

	##INFANTRY##
 	infantry_weapons_1 = 1
	infantry_weapons_2 = 1
	infantry_weapons_improvements_1 = 1
	infantry_weapons_improvements_2 = 1
	infantry_weapons_improvements_3 = 1
	infantry_weapons_improvements_4 = 1
 	basic_infantry_equipment = 1
	support_weapons = 1
	infantry_at = 1
	infantry_at2 = 1
	motorized_infantry = 1
	#infantry_aa = 1
	#infantry_aa2 = 1
	##SUPPORT##
 	tech_support = 1
 	tech_engineers = 1
	tech_engineers_flamethrowers_1 = 1
	tech_recon = 1
	tech_recon2 = 1
	##ARTY##
 	artillery4 = 1
	antiair5 = 1
	##ARMOR##
	advanced_light_tank = 1
	advanced_medium_tank = 1
	mechanised_infantry3 = 1

	APC_1950 = 1
	APC_1950_basic_turret = 1
	#APC_1950_basic_engine = 1

	IFV_1950 = 1
	
	MBT_1950 = 1

	##AIRCRAFT##
	early_fighter = 1
	fighter_1945 = 1
	interceptor_1945 = 1
	CAS_1945 = 1
	##HEAVY AIRCRAFT##
	strategic_bomber1 = 1
	heavy_air_1945 = 1
	tac_bomber1 = 1
	tac_air_1945 = 1
	transport_air1 = 1
	transport_air_1945 = 1
	##ENGINEERING##
	electronic_mechanical_engineering = 1
	radio = 1
	radio_detection = 1
	mechanical_computing = 1
	computing_machine = 1
	basic_encryption = 1
	basic_decryption = 1
	##INDUSTRY##
	ww2_line_production = 1
	batch_production_1 = 1
	energy_technology_1 = 1
    social_construction_1 = 1
	military_construction_1 = 1
}

add_ideas = {
	#Ideas
	Sphere_Fully_Dependent
	VIN_vietcong_idea
	VIN_Crippling_Illiteracy
	VIN_agrarian_society
	VIN_japanese_rubber_exports
	VIN_japanese_army
	#Ministers
	
	#POLITICAL
	tno_political_parties_controlled_opposition
	tno_religious_rights_secularism
	tno_trade_unions_illegal
	tno_immigration_quota_immigration
	tno_slavery_outlawed
	tno_public_meetings_regulated
	tno_press_rights_state_press_only
	tno_vote_franchise_registered_voting
	tno_refugees_vetted_entry
	#MILITARY
	tno_conscription_one_year_draft
	tno_women_military_assistance
	tno_military_supervision_military_policing
	tno_training_basic_training
	tno_racial_integration_integrated_military
	tno_draft_exemptions_religious_deferment
	#SOCIAL
	tno_safety_no_regulations
	tno_health_care_support_for_emergencies
	tno_pollution_few_regulations
	tno_education_public_education
	tno_penal_system_capital_punishment
	tno_lgbt_rights_lgbt_outlawed
	tno_gender_rights_women_in_the_workplace
	tno_minorities_equal_rights
	tno_security_police
	#ECONOMIC
	tno_trade_laws_limited_exports
	tno_income_taxation_elite_tax_exemptions
	tno_minimum_wage_low_minimum_wage
	tno_max_workhours_12_hour_work_day
	tno_child_labor_illegal
	tno_pensions_no_pensions
	tno_unemployment_no_subsidies
	#DEVELOPMENT#
	tno_industrial_expertise_nascent
	tno_industrial_equipment_manufacturing_lines
	tno_agriculture_subsistence
	tno_research_facilities_basic
	tno_academic_base_mass_illiteracy
	tno_army_professionalism_cronyism
	
}

