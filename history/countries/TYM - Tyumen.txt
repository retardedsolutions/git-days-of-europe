﻿capital = 403

# Vacant
recruit_character = TYM_Generic_Vacant



set_country_flag = is_russian_nation

oob = "TYM_1962"

load_focus_tree = tyumen_start

###Playable
set_country_flag = tno_playable_country

set_stability = 0.70

# Country leaders
recruit_character = TYM_Lazar_Kaganovich
recruit_character = TYM_Nikita_Khrushchev

# Minister
recruit_character = TYM_Vyacheslav_Molotov
recruit_character = TYM_Mikhail_Kaganovich
recruit_character = TYM_Ivan_Konev
recruit_character = TYM_Fyodor_Kulakov
recruit_character = TYM_Andrei_Gromyko

# Zlatoust Puppet Government
recruit_character = TYM_Boris_Shcherbina
recruit_character = TYM_Dmitry_Polyansky
recruit_character = TYM_Yelena_Karbysheva
recruit_character = TYM_Ernest_Voznesensky

# Generals
recruit_character = TYM_Filipp_Golikov
recruit_character = TYM_Ivan_Fedyuninsky
recruit_character = TYM_Oleg_Losik
recruit_character = TYM_Vasily_Kuznetsov
recruit_character = TYM_Artyom_Sergeyev
recruit_character = TYM_Alexander_Logunov
recruit_character = TYM_Sergey_Shtemenko
recruit_character = TYM_Pyotr_Lushev


add_ideas = {
	##STANDARD##
	SIB_terror_bombing
	TYM_revisionist_remnant
	TYM_unorthodox_bolshevism
	RUS_warlord_manpower
    RUS_warlord_econ
	RUS_ural_automotive_plant
	RUS_lenin_embalmed
	##MINISTERS##
	TYM_Nikita_Khrushchev_hog
	TYM_Vyacheslav_Molotov_for
	TYM_Mikhail_Kaganovich_eco
	TYM_Ivan_Konev_sec
	##POLITICAL LAWS##
	tno_political_parties_one_party_state
	tno_religious_rights_state_atheism
	tno_trade_unions_state_controlled
	tno_immigration_open_immigration
	tno_slavery_outlawed
	tno_public_meetings_regulated
	tno_press_rights_state_press_only
	tno_vote_franchise_no_voting
	tno_refugees_open
	##MILITARY LAWS###
	tno_conscription_two_year_draft
	tno_women_combat_roles
	tno_military_supervision_military_policing
	tno_training_basic_training
	tno_racial_integration_integrated_military
	tno_draft_exemptions_none
	##SOCIAL LAWS##
	tno_safety_minimal_regulations
	tno_health_care_low_income_protections
	tno_pollution_no_controls
	tno_education_public_education
	tno_penal_system_penal_labor
	tno_lgbt_rights_lgbt_outlawed
	tno_gender_rights_women_in_the_workplace
	tno_minorities_equal_rights
	tno_security_police
	##ECONOMIC LAWS##
	tno_trade_laws_limited_exports
	tno_income_taxation_high_income_weighted
	tno_minimum_wage_low_minimum_wage
	tno_max_workhours_12_hour_work_day
	tno_child_labor_illegal
	tno_pensions_low_pensions
	tno_unemployment_no_subsidies
	##SOCIETAL DEVELOPMENT##
	
	tno_army_professionalism_cronyism
	tno_industrial_expertise_nascent
	tno_industrial_equipment_power_tools
	tno_agriculture_basic_mechanized
	tno_research_facilities_basic
	tno_academic_base_basic_literacy
	tno_admin_efficiency_deficient_administrative_systems
}

### Policy Effectiveness

set_country_flag = tno_defined_startup_effectiveness

### Political
set_variable = { tno_political_parties_effectiveness = 77 }
set_variable = { tno_religious_rights_effectiveness = 66 }
set_variable = { tno_trade_unions_effectiveness = 81 }
set_variable = { tno_immigration_effectiveness = 82 }
set_variable = { tno_slavery_effectiveness = 100 }
set_variable = { tno_public_meetings_effectiveness = 62 }
set_variable = { tno_press_rights_effectiveness = 58 }
set_variable = { tno_vote_franchise_effectiveness = 84 }
set_variable = { tno_refugees_effectiveness = 64 }

### Economic
set_variable = { tno_trade_laws_effectiveness = 34 }
set_variable = { tno_income_taxation_effectiveness = 52 }
set_variable = { tno_minimum_wage_effectiveness = 58 }
set_variable = { tno_max_workhours_effectiveness = 63 }
set_variable = { tno_child_labor_effectiveness = 61 }
set_variable = { tno_pensions_effectiveness = 46 }
set_variable = { tno_unemployment_effectiveness = 43 }

### Social
set_variable = { tno_safety_effectiveness = 43 }
set_variable = { tno_health_care_effectiveness = 42 }
set_variable = { tno_pollution_effectiveness = 46 }
set_variable = { tno_education_effectiveness = 59 }
set_variable = { tno_penal_system_effectiveness = 43 }
set_variable = { tno_lgbt_rights_effectiveness = 89 }
set_variable = { tno_gender_rights_effectiveness = 83 }
set_variable = { tno_minorities_effectiveness = 78 }
set_variable = { tno_security_effectiveness = 48 }

### Military
set_variable = { tno_conscription_effectiveness = 51 }
set_variable = { tno_women_effectiveness = 98 }
set_variable = { tno_military_supervision_effectiveness = 39 }
set_variable = { tno_training_effectiveness = 48 }
set_variable = { tno_racial_integration_effectiveness = 99 }
set_variable = { tno_draft_exemptions_effectiveness = 72 }

set_technology = {
	basic_train = 1

	##INFANTRY##
	infantry_weapons_1 = 1
	infantry_weapons_2 = 1
	infantry_weapons_improvements_1 = 1
	infantry_weapons_improvements_2 = 1
	infantry_weapons_improvements_3 = 1
	infantry_weapons_improvements_4 = 1
	basic_infantry_equipment = 1
	support_weapons = 1
	infantry_at = 1
	motorized_infantry = 1
	#infantry_aa = 1
	##SUPPORT##
	tech_support = 1
	tech_engineers = 1
	tech_engineers_flamethrowers_1 = 1
	tech_recon = 1
	##ARTY##
	
	##ARMOR##

	##AIRCRAFT##
	early_fighter = 1
	fighter_1945 = 1
	interceptor_1945 = 1
	CAS_1945 = 1
	##HEAVY AIRCRAFT##
	strategic_bomber1 = 1
	heavy_air_1945 = 1
	tac_bomber1 = 1
	tac_air_1945 = 1
	transport_air1 = 1
	transport_air_1945 = 1
	##HELICOPTERS##
	helicopter_engines = 1
	very_early_helicopter = 1
	early_helicopter = 1
	##ENGINEERING##
	electronic_mechanical_engineering = 1
	radio = 1
	mechanical_computing = 1
	computing_machine = 1
	##INDUSTRY##
	ww2_line_production = 1
	batch_production_1 = 1
	building_construction_1 = 1
	resource_extraction_methods = 1
	excavation0 = 1
	energy_technology_1 = 1
	social_construction_1 = 1
	administration_construction_1 = 1
	military_construction_1 = 1
	production_unit_gain_1 = 1
	production_unit_gain_2 = 1
}

# NSB tech #

if = {
	limit = {
		NOT = { has_dlc = "No Step Back" }
	}
	set_technology = {
		advanced_light_tank = 1
		advanced_medium_tank = 1
		mechanised_infantry3 = 1

		APC_1950 = 1

		IFV_1950 = 1

		MBT_1950 = 1

		artillery4 = 1
		antiair5 = 1
	}
}
if = {
	limit = {
		has_dlc = "No Step Back"
	}
	set_technology = {
		NSB_APC_1945 = 1
		NSB_APC_1950 = 1

		NSB_IFV_1945 = 1
		NSB_IFV_1945_modules_1 = 1
		NSB_IFV_1945_modules_2 = 1
		NSB_IFV_1950 = 1

		NSB_MBT_1945 = 1
		NSB_MBT_1945_modules_1 = 1
		NSB_MBT_1945_modules_2 = 1
		NSB_MBT_1950 = 1

		NSB_Armour_1 = 1
		NSB_Engine_1 = 1

		NSB_artillery_1945 = 1

		NSB_antiair_1945 = 1
	}
}


set_politics = {
	ruling_party = communist
	last_election = "1933.3.5"
	election_frequency = 48
	elections_allowed = no

}

set_popularities = {
	communist = 68
	ultranationalism = 9
	socialist = 13
	social_democracy = 0
	liberal_democracy = 0

	conservative_democracy = 0
	authoritarian_democracy = 10
	despotism = 0
	fascism = 0
	
}

# NSB #
if = {
	limit = {
		has_dlc = "No Step Back"
	}
	create_equipment_variant = {
		name = "T-34-85"
		type = MBT_chassis_0
		parent_version = 0
		modules = {
			main_armament_slot = MBT_cannon
			turret_type_slot = MBT_three_man_tank_turret
			suspension_type_slot = tank_torsion_bar_suspension
			armor_type_slot = tank_welded_armor
			engine_type_slot = tank_diesel_engine
			special_type_slot_1 = secondary_turret_hmg
		}
		icon = GFX_AMR_advanced_medium_tank_medium
		upgrades = {
			tank_nsb_engine_upgrade = 6
			tank_nsb_armor_upgrade = 6
		}
	}

	create_equipment_variant = {
		name = "PT-76"
		type = IFV_chassis_1
		parent_version = 0
		modules = {
			main_armament_slot =IFV_cannon
			turret_type_slot = IFV_two_man_tank_turret
			suspension_type_slot = tank_torsion_bar_suspension
			armor_type_slot = tank_welded_armor
			engine_type_slot = tank_diesel_engine
			special_type_slot_1 = command_optics
		}
		
		icon = GFX_KOM_IFV_1950_medium
		upgrades = {
			tank_nsb_engine_upgrade = 3
			tank_nsb_armor_upgrade = 2
		}
	}

	create_equipment_variant = {
		name = "T-70"
		type = IFV_chassis_0
		parent_version = 0
		modules = {
			main_armament_slot =IFV_cannon
			turret_type_slot = IFV_one_man_tank_turret
			suspension_type_slot = tank_torsion_bar_suspension
			armor_type_slot = tank_welded_armor
			engine_type_slot = tank_gasoline_engine
		}
		
		icon = GFX_AMR_advanced_light_tank_medium
		upgrades = {
			tank_nsb_engine_upgrade = 4
			tank_nsb_armor_upgrade = 4
		}
		obsolete = yes
	}
}
