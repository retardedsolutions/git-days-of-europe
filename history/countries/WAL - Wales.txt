﻿capital = 122

# Vacant
recruit_character = WAL_Generic_Vacant

# Leaders
recruit_character = WAL_saunders_lewis
recruit_character = WAL_emrys_thomas
recruit_character = WAL_john_morris
recruit_character = WAL_john_barnard_jenkins
recruit_character = WAL_julian_cayo_evans
recruit_character = WAL_raymond_williams
recruit_character = WAL_john_roberts
recruit_character = WAL_welsh_assembly
recruit_character = WAL_leaderless

# Generals
recruit_character = WAL_morgan_llewellyn
recruit_character = WAL_meredith_thomas
recruit_character = WAL_peter_raymond_leuchars
recruit_character = WAL_john_chaston
recruit_character = WAL_geoffrey_inkin
recruit_character = WAL_jock_lewes
recruit_character = WAL_george_taylor
recruit_character = WAL_dennis_coslett
recruit_character = WAL_stephan_beattie
recruit_character = WAL_john_wallace_linton

# Ministers
recruit_character = WAL_edward_millward
recruit_character = WAL_emrys_roberts
recruit_character = WAL_gwynfor_evans
recruit_character = WAL_niclas_y_glais
recruit_character = WAL_ithel_davies
recruit_character = WAL_arthur_lewis_horner
recruit_character = WAL_nicholas_edwards
recruit_character = WAL_morys_bruce
recruit_character = WAL_hugh_jenkins
recruit_character = WAL_gwilym_lloyd_george
recruit_character = WAL_roderic_bowden
recruit_character = WAL_owain_williams


oob = "WAL_1962"

###Playable
set_country_flag = tno_playable_country

set_country_flag = tno_defined_startup_effectiveness
set_variable = { tno_trade_laws_effectiveness = 80 }
set_variable = { tno_income_taxation_effectiveness = 65 }
set_variable = { tno_minimum_wage_effectiveness = 70 }
set_variable = { tno_max_workhours_effectiveness = 75 }
set_variable = { tno_child_labor_effectiveness = 80 }
set_variable = { tno_pensions_effectiveness = 40 } 
set_variable = { tno_unemployment_effectiveness = 100 }

set_variable = { tno_safety_effectiveness = 45 }
set_variable = { tno_health_care_effectiveness = 45 }
set_variable = { tno_pollution_effectiveness = 45 }
set_variable = { tno_education_effectiveness = 55 }
set_variable = { tno_penal_system_effectiveness = 70 }
set_variable = { tno_lgbt_rights_effectiveness = 60 }
set_variable = { tno_gender_rights_effectiveness = 40 }
set_variable = { tno_minorities_effectiveness = 65 }
set_variable = { tno_security_effectiveness = 55 }

set_variable = { tno_political_parties_effectiveness = 20 }
set_variable = { tno_religious_rights_effectiveness = 85 }
set_variable = { tno_trade_unions_effectiveness = 40 }
set_variable = { tno_immigration_effectiveness = 40 }
set_variable = { tno_slavery_effectiveness = 100 }
set_variable = { tno_public_meetings_effectiveness = 60 }
set_variable = { tno_press_rights_effectiveness = 60 }
set_variable = { tno_vote_franchise_effectiveness = 70 }
set_variable = { tno_refugees_effectiveness = 50 }

set_variable = { tno_conscription_effectiveness = 60 }
set_variable = { tno_women_effectiveness = 45 }
set_variable = { tno_military_supervision_effectiveness = 75 }
set_variable = { tno_training_effectiveness = 50 }
set_variable = { tno_racial_integration_effectiveness = 60 }
set_variable = { tno_draft_exemptions_effectiveness = 55 }

add_ideas = {
	WAL_Welsh_CounterCulture
	WAL_Free_Welsh_Army
	#welsh_counterculture
	#free_welsh_army
	WAL_welsh_coal_income_idea_dummy
	
	#political#
	tno_political_parties_multiparty_system
	tno_religious_rights_secularism
	tno_trade_unions_all_allowed
	tno_immigration_quota_immigration
	tno_slavery_outlawed
	tno_public_meetings_allowed
	tno_press_rights_censored_press
	tno_vote_franchise_universal
	tno_refugees_vetted_entry
	#military#
	tno_conscription_two_year_draft
	tno_women_military_assistance
	tno_military_supervision_rules_of_engagement
	tno_training_basic_training
	tno_racial_integration_integrated_military
	tno_draft_exemptions_civil_service_deferment
	#economic#
	tno_trade_laws_limited_exports
	tno_income_taxation_flat_taxes
	tno_minimum_wage_low_minimum_wage
	tno_max_workhours_12_hour_work_day
	tno_child_labor_illegal
	tno_pensions_acceptable_pensions
	tno_unemployment_no_subsidies
	#social#
	tno_safety_minimal_regulations
	tno_health_care_low_income_protections
	tno_pollution_few_regulations
	tno_education_public_education
	tno_penal_system_incarceration
	tno_lgbt_rights_lgbt_outlawed
	tno_gender_rights_women_in_the_workplace
	tno_minorities_equal_rights
	tno_security_police
	#social development
	tno_academic_base_secondary_schooling
	tno_research_facilities_modern
	tno_agriculture_mass_mechanized
	tno_industrial_equipment_factory_complexes
	tno_industrial_expertise_experienced
	tno_army_professionalism_professional
	
	tno_admin_efficiency_deficient_administrative_systems
	#MINISTERS#
	WAL_Emrys_Thomas_dep
	WAL_Julian_Cayo-Evans_sec
	WAL_Emrys_Roberts_for
	WAL_Gwynfor_Evans_eco
}

set_technology = {
	basic_train = 1

	##INFANTRY##
	# infantry_equipment_0 = 1
	infantry_weapons_1 = 1
	infantry_weapons_2 = 1
	infantry_weapons_improvements_1 = 1
	infantry_weapons_improvements_2 = 1
	infantry_weapons_improvements_3 = 1
	basic_infantry_equipment = 1
	support_weapons = 1
	support_weapons2 = 1
	night_vision_1 = 1
	infantry_at = 1
	infantry_at2 = 1
	motorized_infantry = 1
	motorized_infantry_1 = 1
	
	##SUPPORT##
	tech_support = 1
	tech_engineers = 1
	tech_recon = 1
	
	##ARMOR##
	advanced_light_tank = 1
	mechanised_infantry3 = 1
	advanced_medium_tank = 1
	
	##ARTILLERY##
	artillery4 = 1
	antiair5 = 1
	
	##NAVAL##
	ww2_destroyer = 1
	ww2_submarine = 1
	ww2_cruiser = 1
	basic_submarine = 1
	transport = 1
	
	##HELICOPTERS##
	very_early_helicopter = 1
 	early_helicopter = 1
	
	##LIGHT AIR##
	early_fighter = 1
	fighter_1945 = 1
	interceptor_1945 = 1
	
	##HEAVY AIR##
	tac_bomber1 = 1
	
	##ENGINEERING##
	electronic_mechanical_engineering = 1
	radio = 1
	radio_detection = 1
	decimetric_radar = 1
	centimetric_radar = 1
	mechanical_computing = 1
	computing_machine = 1
	basic_encryption = 1
	basic_decryption = 1
	improved_computing_machine = 1
	improved_encryption = 1
	improved_decryption = 1
	advanced_computing_machine = 1
	advanced_encryption = 1
	advanced_decryption = 1
	helicopter_engines = 1
	jet_engines = 1
	
	##INDUSTRY##
	resource_extraction_methods = 1
	ww2_line_production = 1
	excavation0 = 1
	building_construction_1 = 1
	production_unit_gain_1 = 1
	production_unit_gain_2 = 1
	consumer_goods_reduction_1 = 1
	consumer_goods_reduction_2 = 1
	social_construction_1 = 1
	military_construction_1 = 1
	administration_construction_1 = 1
	energy_technology_1 = 1	
	##INDUSTRY TECHS ARE WIP##
	
}
add_to_array = { WAL_POL_RULFAC = 0 }
resize_array = { WAL_POL_RULFAC = 6 }
set_variable = { WAL_POL_RULFAC^0 = 1}
set_variable = { WAL_POL_RULFAC^1 = 2}
set_variable = { WAL_POL_RULFAC^2 = 3}
set_variable = { WAL_POL_RULFAC^3 = 5}
set_variable = { WAL_POL_RULFAC^4 = 4}
set_variable = { WAL_POL_RULFAC^5 = 6}
load_focus_tree = WAL_Initial_Tree

set_research_slots = 2
set_stability = 0.7
set_war_support = 0.65

set_politics = {	
	ruling_party = authoritarian_democracy
	last_election = "1960.11.14"
	election_frequency = 48
	elections_allowed = yes ##suspended through duration of war, which is handled via event
}

set_popularities = {
    communist = 0
    national_socialism = 0
    socialist = 5
    social_democracy = 22
    liberal_democracy = 0

    conservative_democracy = 11
    authoritarian_democracy = 42
    despotism = 17
    fascism = 3
    ultranationalism = 0
    
}

if = {
	limit = {
		has_dlc = "Man the Guns"
	}
	create_equipment_variant = {
		name = "H Class"
		type = ship_hull_light_1
		name_group = GER_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_2
			fixed_ship_fire_control_system_slot = ship_fire_control_system_2
			fixed_ship_radar_slot = ship_radar_2
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			mid_1_custom_slot = ship_light_battery_1
			rear_1_custom_slot = ship_anti_air_2
    	}
	}

	create_equipment_variant = {
		name = "S Class"
		type = ship_hull_submarine_1
		name_group = GER_SS_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_sub_1
			rear_1_custom_slot = ship_torpedo_sub_1
    	}
    }

    create_equipment_variant = {
		name = "Town Class"
		type = ship_hull_cruiser_1
		name_group = GER_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_2
			fixed_ship_anti_air_slot = ship_anti_air_2
			fixed_ship_fire_control_system_slot = ship_fire_control_system_3
			fixed_ship_radar_slot = ship_radar_3
			fixed_ship_engine_slot = cruiser_ship_engine_2
			fixed_ship_secondaries_slot = ship_secondaries_2
			fixed_ship_armor_slot = ship_armor_cruiser_2
			mid_1_custom_slot = ship_light_battery_2
			rear_1_custom_slot = ship_anti_air_2
    	}
	}

	set_naval_oob = "WAL_1962_naval"	
}

