capital = 1534

# Vacant
recruit_character = KLT_Generic_Vacant
fill_empty_minister_nochecks = yes # Please remove if you're adding ministers.
recruit_character = KLT_mir_ahmad_yar_khan
recruit_character = KLT_sher_mohammed_marri
recruit_character = KLT_mir_gul_khan_nasseer
recruit_character = KLT_khair_bakhsh_marri
recruit_character = KLT_abdul_wali
add_manpower = 40000

oob = "KLT_1962"

set_technology = {
	basic_train = 1

 	##INFANTRY##
 	infantry_weapons_1 = 1
 	infantry_weapons_2 = 1
 	infantry_weapons_improvements_1 = 1
 	infantry_weapons_improvements_2 = 1
 	basic_infantry_equipment = 1
 	support_weapons = 1
 	infantry_at = 1
 	motorized_infantry = 1
    tech_mountaineers = 1

 	##SUPPORT##
 	tech_support = 1
 	tech_engineers = 1
 	tech_engineers_flamethrowers_1 = 1
 	tech_recon = 1
 	tech_recon2 = 1
 	tech_logistics_company = 1

 	##ARTY##
 	antiair5 = 1
 	artillery4 = 1

 	##LIGHT AIR##
 	early_fighter = 1
 	fighter_1945 = 1

 	##ENGINEERING##
 	electronic_mechanical_engineering = 1
 	radio = 1
 	mechanical_computing = 1
 	computing_machine = 1
 	basic_encryption = 1
 	basic_decryption = 1
 	improved_computing_machine = 1
 	improved_encryption = 1
 	advanced_computing_machine = 1

	##INDUSTRY##
	ww2_line_production = 1
	resource_extraction_methods = 1
    excavation0 = 1
	batch_production_1 = 1
}

set_politics = {
	ruling_party = authoritarian_democracy
	last_election = "1960.8.29"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    communist = 15
    socialist = 10
    social_democracy = 0
    liberal_democracy = 0
    conservative_democracy = 0
    authoritarian_democracy = 45
    despotism = 30
    fascism = 0
    national_socialism = 0
    ultranationalism = 0
    
}

add_ideas = {
	#STARTING#
	AFG_Westernising_Nation
	#POLITICAL
	tno_political_parties_controlled_opposition
	tno_religious_rights_state_religion
	tno_trade_unions_illegal
	tno_immigration_quota_immigration
	tno_slavery_outlawed
	tno_public_meetings_regulated
	tno_press_rights_censored_press
	tno_vote_franchise_no_voting
	tno_refugees_vetted_entry
	#MILITARY
	tno_conscription_two_year_draft
	tno_women_banned
	tno_military_supervision_military_policing
	tno_training_basic_training
	tno_racial_integration_not_applicable
	tno_draft_exemptions_religious_deferment
	#SOCIAL
	tno_safety_no_regulations
	tno_health_care_support_for_emergencies
	tno_pollution_no_controls
	tno_education_elite_only
	tno_penal_system_capital_punishment
	tno_lgbt_rights_lgbt_outlawed
	tno_gender_rights_traditional_roles
	tno_minorities_oppression
	tno_security_data_cohesion
	#ECONOMIC
	tno_trade_laws_limited_exports
	tno_income_taxation_elite_tax_exemptions
	tno_minimum_wage_no_minimum_wage
	tno_max_workhours_unlimited_work_day
	tno_child_labor_legal
	tno_pensions_no_pensions
	tno_unemployment_no_subsidies
	#DEVELOPMENT#
	tno_industrial_expertise_nascent
	tno_industrial_equipment_manufacturing_lines
	tno_agriculture_subsistence
	tno_research_facilities_basic
	tno_academic_base_mass_illiteracy
	tno_army_professionalism_cronyism
}
